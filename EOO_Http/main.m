//
//  main.m
//  EOO_Http
//
//  Created by EOO Sweden on 13/11/13.
//  Copyright (c) 2013 EOO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EOOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EOOAppDelegate class]));
    }
}
