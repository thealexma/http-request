//
//  EOOViewController.h
//  EOO_Http
//
//  Created by EOO Sweden on 13/11/13.
//  Copyright (c) 2013 EOO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface EOOViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *fieldUrl;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITextView *textViewForJSON;

@end
