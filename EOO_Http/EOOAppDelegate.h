//
//  EOOAppDelegate.h
//  EOO_Http
//
//  Created by EOO Sweden on 13/11/13.
//  Copyright (c) 2013 EOO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
