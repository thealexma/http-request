//
//  EOOViewController.m
//  EOO_Http
//
//  Created by EOO Sweden on 13/11/13.
//  Copyright (c) 2013 EOO. All rights reserved.
//

#import "EOOViewController.h"

@interface EOOViewController ()

@end

@implementation EOOViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)tapBackground:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)fetchClick:(id)sender {
    // Post data
    NSString *userURL = self.fieldUrl.text;
    
    NSString *http = @"http://";
    if ([userURL rangeOfString:http].location == NSNotFound) {
        userURL = [NSString stringWithFormat:@"http://%@",userURL];
        self.fieldUrl.text = userURL;
    }
    
    NSURL* url = [NSURL URLWithString:userURL];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDidFinishSelector:@selector(fetchDone:)];
    [request setDelegate:self];
    [request addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [request addRequestHeader:@"Accept-Language" value:@"sv-se,sv,en"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request startAsynchronous];
}

- (void)fetchDone:(ASIHTTPRequest *)request{
    // Put the result in a webview
    [self.webView loadHTMLString:request.responseString baseURL:request.url];
    
    // Clear textview
    self.textViewForJSON.text = @"";
    
    NSError* error;
    NSDictionary* result = [NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:&error];
    
    // Log result
    NSLog(@"%@",result);
    
    // Assuming we get an array of dictionaries
    NSArray* array1 = [result valueForKey:@"data"];
    
    for(NSDictionary* dic in array1){
        for(NSString* key in dic){
            NSString* value = [dic valueForKey:key];
            NSLog(@"%@ = %@", key, value);
            self.textViewForJSON.text = [self.textViewForJSON.text stringByAppendingString:[NSString stringWithFormat:@"%@: %@ \n", key, value]]; // \n = new line
        }
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self fetchClick:nil];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
